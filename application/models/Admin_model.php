<?php
class Admin_model extends CI_Model
{
    function addUser($table,$data){
        $this->db->insert($table, $data);
        return true;
    }

    function getUserById($table,$id){
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        $data = $query->row_array();
        return $data;
    }

    function updateUser($table,$data,$id){
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        return true;
    }

    function deleteUser($table,$id){
        $this->db->delete($table, array('id' => $id)); 
        return true;
    }
 

    public function update($data,$table,$field_name, $id) {
         
        $this->db->where($field_name,$id);
        $this->db->set($data);
        return $this->db->update($table);
    }

    public function delete($table,$field_name,$id){
        $this->db->where($field_name, $id);
        return $this->db->delete($table);
    }

      function get_allrecords($table='') {
        $this->db->select('*');       
        $query = $this->db->get($table);
        $data = $query->result();
        return $data;
    }
      
    public function execute_query($query=''){

        $query = $this->db->query($query);
        return $query->result();
    }

    function login($username, $password){ 

        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('email', $username);
        $this->db->where('password', ($this->isValidMd5($password)) ? $password : MD5($password));
        $this->db->limit(1);
        $query = $this->db->get();      
        if($query->num_rows() == 1)
        {
            if(strtolower($query->result()[0]->status)==1)
            {
                return $query->result();
            }else{

                return 1;
            }
        } 
        else
        {
            return false;
        }
    }

    public function isValidMd5($md5 =''){
        return preg_match('/^[a-f0-9]{32}$/', $md5);
    } 


    function checkRegister($email) {
        $this->db->select('*');
        $this->db->where('email', $email);
        $query = $this->db->get('admin');
        $data = $query->num_rows();
        if($data=="0")
        return 'No';
        else
        return 'Yes';
    }
    

    function checkRegisteremail($email,$id) {
        $this->db->select('*');
        $this->db->where('email', $email);
        $this->db->where('id != ', $id);
        $query = $this->db->get('admin');
        $data = $query->num_rows();
        if($data=="0")
        return 'No';
        else
        return 'Yes';
    }

    function checkdataexist($table,$field,$value,$id) {
        $this->db->select('*');
        $this->db->where($field, $value);
        $this->db->where('id != ', $id);
        $query = $this->db->get($table);
        $data = $query->num_rows();
        if($data=="0")
        return 'No';
        else
        return 'Yes';
    }
    
    function getAllLogs($data = array())
    {
    $query = $this->db->get('logs');

    $columnArray = array (
        "date_time",
        "user",
        "action",
        "ip");
        $this->db->select('*');
        $this->db->from('logs');
        if (isset($data['search']['value']) && $data['search']['value'] != '') {

        $this->db->or_where(
        'id',
        $data['search']['value']
        );
        $this->db->or_where(
        'date_time like "%'.$data['search']['value'].'%"'
        );
        $this->db->or_where(
        'user like "%'.$data['search']['value'].'%"'
        );
        $this->db->or_where(
        'action like "%'.$data['search']['value'].'%"'
        );
        $this->db->or_where(
        'ip like "%'.date('Y-m-d H:i:s', strtotime($data['search']['value'])).'%"'
        );
    }

    if (isset($data['order'])) {
        $this->db->order_by(
        $columnArray[$data['order'][0]['column']],
        $data['order'][0]['dir']
        );
        }
        $this->db->limit($data['length'], $data['start']);
        $query = $this->db->get();  
        return $query->result_array();
    }

    function getAllLogsCount($data = array())
    {
        $query = $this->db->get('logs');
        $this->db->select('COUNT(*) AS aggregate');
        $this->db->from('logs');
        if (isset($data['search']['value']) && $data['search']['value'] != '') {

        $this->db->or_where(
        'id',
        $data['search']['value']
        );
        $this->db->or_where(
        'date_time like "%'.$data['search']['value'].'%"'
        );
        $this->db->or_where(
        'user like "%'.$data['search']['value'].'%"'
        );
        $this->db->or_where(
        'action like "%'.$data['search']['value'].'%"'
        );
        $this->db->or_where(
        'ip like "%'.date('Y-m-d H:i:s', strtotime($data['search']['value'])).'%"'
        );
    }   

    $query = $this->db->get();  
    return $query->result_array();
    }

    function addLog($data) {
    $this->db->insert('logs', $data);
    return true;
    }


    function marketplaceid_list(){

      $marketplaceid =  array('A2EUQ1WTGCTBG2' => 'CA',
            'A1AM78C64UM0Y8'=>'MX',
            'ATVPDKIKX0DER'=>'USA',            
            'A2Q3Y263D00KWC'=>'BR', 
            'A1PA6795UKMFR9'=>'DE',
            'A1RKKUPIHCS9HS'=>'ES',
            'A13V1IB3VIYZZH'=>'FR',
            'APJ6JRA9NG5V4'=>'IT',            
            'A1F83G8C2ARO7P'=>'UK', 
            'A21TJRUUN4KGV'=>'IN',
            'A1VC38T7YXB528'=>'JP', 
            'AAHKV2X7AFYLW' =>'CN');  
             
        return $marketplaceid;

    }

    function get_last_record($table=''){

        $this->db->select('*');
        $this->db->from($table);
        $this->db->group_by('id'); 
        $this->db->limit(1); 
        $query = $this->db->get();         
        $result = $query->result_array();
        return $result[0];
    }


    function get_single_record($table,$field,$field_value,$status='N'){
        $this->db->select('*');
        $this->db->where($field,$field_value);
        if($status == 'Y'){            
            $this->db->where('status',1);
        }
        $query = $this->db->get($table);
        $data = $query->row_array();
        return $data;
    }

}
