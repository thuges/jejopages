<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct() {

        parent::__construct();
        
        \Stripe\Stripe::setApiKey("sk_test_Si9FHvMxgvLVP7ujO9oVkoDx");
        
        $this->load->model('admin_model','',TRUE);
        $this->load->library('layout');         
        $this->load->library('session');         
        $this->load->helper('file');

        $temp = $this->admin_model->get_allrecords('timezone');

        if(function_exists( 'date_default_timezone_set' ) ) {
            date_default_timezone_set($temp[0]->timezone_name);
        }

    }
   
    public function index(){
        
        if(!$this->session->userdata('logged_in')) {  
            redirect('login','refresh');    
        }

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Dashboard');
        $info['data'] = $_POST;
 
        $this->layout->view('admin/dashboard',$info); 
    }

    public function login(){

        if ($this->session->userdata('logged_in')) {  
            redirect('dashboard','refresh');    
        }

        $this->layout->title('Jejo Pages - Login');        
        $this->load->view('layouts/loginlayout.php'); 
    }
     
    public function create_text_file(){

        if(!$this->session->userdata('logged_in')) {  
            redirect('login','refresh');    
        }

        try {
            $amz = new AmazonReportList(""); //store name matches the array key in the config file   
            $amz->fetchReportList();
                
            $amzz = new AmazonReport(""); 
            $amzz->setReportId('');
            $amzz->fetchReport();
            // $myfile = fopen("/assets/file.txt", "rw");        
            // $amzz->saveReport("/assets/file.txt"); 

            $data = $amzz->getLastResponse(); 
         
        } catch (Exception $ex) {
            echo 'There was a problem with the Amazon library. Error: '.$ex->getMessage();
        } 

    }

    public function adduser(){
         $this->is_login();

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Add User'); 

        $this->layout->view('admin/user_accounts'); 
    }

    public function user_accounts(){
            
        $this->is_login();

         if(!$this->session->flashdata('error')) {
            $this->session->unset_userdata('first_name');         
            $this->session->unset_userdata('last_name');         
            $this->session->unset_userdata('display_name');          
            $this->session->unset_userdata('role');          
            $this->session->unset_userdata('email'); 
            $this->session->unset_userdata('status'); 
        }

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Add User'); 
        $data['user_detail'] = $this->admin_model->get_allrecords('admin');    
        $this->layout->view('admin/user_list',$data); 

    }

     
    function user_add() {
         
         $this->is_login();

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo pages - User');
        
         if($this->input->post('submit')) {

            $email=$this->input->post('email');

            $checkmail=$this->admin_model->checkRegister($email);  

            if($checkmail == 'No')
            {               
                $data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name' => $this->input->post('lastname'),
                    'display_name' => $this->input->post('displayname'),
                    'email' => $this->input->post('email'),
                    'password' => md5($this->input->post('password')),                    
                    'role' => $this->input->post('role'),
                    'status' => '1'
                );                 
                 
                $last_user_id = $this->admin_model->addUser('admin',$data);
                $this->session->set_flashdata("message","<font class='success'>User successfully added.</font>");

                redirect('admin/user_list');

            } else  {

                $current_content = $this->session->set_userdata(array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name' => $this->input->post('lastname'),
                    'display_name' => $this->input->post('displayname'),
                    'email' => $this->input->post('email'), 
                    'role' => $this->input->post('role'),
                    'status' => '1'
                ));
                
                $this->session->set_flashdata("error","<font class='error'>Email id already exists</font>");
                redirect('admin/adduser');
            }
        }

        $this->layout->view('admin/user_list');
    }

    function profile_edit(){   

        $this->is_login();

         $session_data = $this->session->userdata('logged_in');
         if($session_data['id'] != ''){
            $id = $session_data['id'];
         }

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Edit Profile');        
        $data['user'] = $this->admin_model->getUserById('admin',$id);

         if($this->input->post('submit')){

            $email = $this->input->post('email');
             $checkmail=$this->admin_model->checkRegisteremail($email,$id);
            if($checkmail == 'No') {                 
               
                $data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name' => $this->input->post('lastname'),
                    'display_name' => $this->input->post('displayname'),
                    'email' => $this->input->post('email'),                                        
                    'role' => $this->input->post('role'),
                    'status' => '1'
                );
         
                if($this->input->post('password') AND $this->input->post('password') != '')  {
                    $data1=array('password' => md5($this->input->post('password')));
                    $this->admin_model->updateUser('admin',$data1,$id);
                }  

                $this->admin_model->updateUser('admin',$data,$id);    
                $this->session->set_flashdata("message","<font class='success'>User successfully Updated.</font>");

                redirect('admin/profile_edit');
            }  else {
                $this->session->set_flashdata("error","<font class='error'>Email id already exists</font>");
                redirect('admin/profile_edit');
            }
        }
                
        $this->layout->view('admin/profile_edit', $data);
   }

   function profile_update($id='') {       
 
        $this->is_login();

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Edit Profile');        
        $data['user'] = $this->admin_model->getUserById('admin',$id);

         if($this->input->post('submit')){

            $email = $this->input->post('email');
             $checkmail=$this->admin_model->checkRegisteremail($email,$id);
            if($checkmail == 'No') {                 
               
                $data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name' => $this->input->post('lastname'),
                    'display_name' => $this->input->post('displayname'),
                    'email' => $this->input->post('email'),                                        
                    'role' => $this->input->post('role'),
                    'status' => '1'
                );
         
                if($this->input->post('password') AND $this->input->post('password') != '')  {
                    $data1=array('password' => md5($this->input->post('password')));
                    $this->admin_model->updateUser('admin',$data1,$id);
                }  

                $this->admin_model->updateUser('admin',$data,$id);    
                $this->session->set_flashdata("message","<font class='success'>User successfully Updated.</font>");

                redirect('admin/profile_edit');
            }  else {
                $this->session->set_flashdata("error","<font class='error'>Email id already exists</font>");
                redirect('admin/profile_edit');
            }
        }
                
        $this->layout->view('admin/profile_edit', $data);
    }

    
    function user_edit($id='') {       
 
        $this->is_login();

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Edit User');        
        $data['user'] = $this->admin_model->getUserById('admin',$id);

         if($this->input->post('submit')){

            $email = $this->input->post('email');
             $checkmail=$this->admin_model->checkRegisteremail($email,$id);
            if($checkmail == 'No') {                 
               
                $data = array(
                    'first_name' => $this->input->post('firstname'),
                    'last_name' => $this->input->post('lastname'),
                    'display_name' => $this->input->post('displayname'),
                    'email' => $this->input->post('email'),                                        
                    'role' => $this->input->post('role'),
                    'status' => '1'
                );
         
                if($this->input->post('password') AND $this->input->post('password') != '')  {
                    $data1=array('password' => md5($this->input->post('password')));
                    $this->admin_model->updateUser('admin',$data1,$id);
                }  

                $this->admin_model->updateUser('admin',$data,$id);    
                $this->session->set_flashdata("message","<font class='success'>User successfully Updated.</font>");

                redirect('admin/user_list');
            }  else {
                $this->session->set_flashdata("error","<font class='error'>Email id already exists</font>");
                redirect('admin/user_edit/'.$id);
            }
        }
                
        $this->layout->view('admin/user_edit', $data);
    }


    function user_delete($id) {
         $this->is_login();
        if (!$this->session->userdata('logged_in')) {
            $this->loginCheck();
        }
         $this->admin_model->deleteUser('admin',$id);
         $this->session->set_flashdata("message","<font class='success'>User deleted successfully.</font>");
         redirect('admin/user_list');
    }

    function amazon_account_delete($id) {
         $this->is_login();
        if (!$this->session->userdata('logged_in')) {
            $this->loginCheck();
        }
         $this->admin_model->deleteUser('amazon_mws_accounts',$id);
         $this->session->set_flashdata("message","<font class='success'>Account deleted successfully.</font>");
         redirect('admin/amazon-accounts');
    }

    public function verifylogin()  {

 
        $result = $this->admin_model->login($_POST['username'], $_POST['password']);       
        if(!empty($result)) {
             
            if($result==1){

                $this->session->set_flashdata("error","<font class='error'>Your account is not active.</font>");
                 redirect('login','refresh');   
            }
            $sess_array = array();
            foreach($result as $row)
            {   
                $role ='';
                if($row->role == 1){
                    $role = 'User';
                } else {
                    $role = 'Administrator';
                }

                $sess_array = array(
                'id' => $row->id,
                'email' => $row->email,
                'username' => $row->first_name,               
                'role' => $role,               
                'display_name' => $row->display_name                
                );
                $this->session->set_userdata('logged_in', $sess_array);
                echo '<script type="text/javascript">document.location="'.base_url().'dashboard";</script>';
                //$this->load->view('index');
            }
            
            $this->user_log('Successful login');

        } else{
 
            $this->user_log('Unsuccessful login');
            $this->session->set_flashdata("error","<font class='error'> :( username and/or password is incorrect</font>");
            redirect('login','refresh');   
        }
    }

    function logout() {
        
        $this->user_log('Logout');
        $this->session->unset_userdata('logged_in');              
        redirect('login','refresh');
    }
      

    public function is_login(){        
        if(!$this->session->userdata('logged_in')) {  
            redirect('login','refresh');    
        }
    }

    public function userlog() {  

        $this->is_login();
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - User Access Log');
        $this->layout->view('admin/logs');
    }

    public function logsRecords() {

           $this->is_login();

           $get = $this->input->get();

           $countData = $this->admin_model->getAllLogsCount($get);

           $data['draw'] = $get['draw'];

           $data['recordsTotal'] = $countData[0]['aggregate'];

           $data['recordsFiltered'] = $countData[0]['aggregate'];

           $data['data'] = $this->admin_model->getAllLogs($get);

           echo json_encode($data);
           exit;

    }

    public function timezone() {  
        $this->is_login();
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Timezone');       
        $data['timezone'] = $this->admin_model->get_allrecords('timezone'); 
        $this->layout->view('admin/timezone',$data);
    }

    function update_timezone(){ 
        $data['timezone'] = $_POST['timezone'];
        $data['timezone_name'] = $_POST['timezone_name'];
        $this->admin_model->update($data,'timezone','id',1);
         $this->session->set_flashdata("message","<font class='success'>Timezone update successfully.</font>");
        redirect('admin/timezone','refresh');
    }

    public function amazonmws(){
        
        $this->is_login();

        if(!$this->session->flashdata('error')) {            
            $this->session->unset_userdata('serviceurl');         
            $this->session->unset_userdata('seller_id');
            $this->session->unset_userdata('authtoken');
            
        }
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Amazon MWS  ');        
        $this->layout->view('admin/amazonmws');
    }

    public function check_amazon_auth() {  

        $this->is_login();
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Amazon MWS');     

        $this->load->helper('path'); 
        $path = set_realpath(FCPATH.'amazon-config.php',true);
        $log_path = set_realpath(FCPATH.'log.txt',true);
         
        $amz = new AmazonReportList('JejoPages','','',$path); //store name matches the array key in the config file   // ""
        $amz->setLogPath($log_path);
        $amz->setConfig($path);
        $amz->fetchReportList();
       
        $rslt = $amz->getLastResponse();
        $myfile = fopen("/var/www/html/jejopages/log_amazon.txt", "a");
        $writeLog = date("Y-m-d H:i:s")."\n";
        $writeLog.= print_r($rslt, true);
        $writeLog.="===========================\n";
        $file = fopen("/var/www/html/jejopages/log_amazon.txt","a");
        fwrite($file,$writeLog);
        fclose($file);


         $data['result'] = '';
         $data['code'] = $rslt['code'];
        if($rslt['code'] == 200){
                $detail['serviceurl'] = $this->input->post('serviceurl');   
                $detail['seller_id'] = $this->input->post('sellerid');                   
                $detail['authtoken'] = $this->input->post('authtoken');   
                $detail['status'] = 1;
                $this->admin_model->addUser('amazon_mws_accounts',$detail);
                $this->user_log('Step 4 - Credentials validated successfully');
                $this->session->set_flashdata("message","<font class='success'>Credentials validated successfully.
</font>");
                redirect('admin/amazon-accounts');
        } else  {

            $amazon_content = $this->session->set_userdata(array(
        
            'serviceurl' => $this->input->post('serviceurl'),
            'seller_id' => $this->input->post('sellerid'),
            'authtoken' => $this->input->post('authtoken')));
            $this->user_log('Step 4 - Error validating credentials');
            $this->session->set_flashdata("error","Error validating credentials. Please double check if they're correct and if so, please contact support.");
        }

        redirect('admin/amazonmws','refresh');
    }

    public function subscription(){
        $this->is_login();
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Subscription'); 
        $session_data = $this->session->userdata('logged_in'); 
 
        $subscrib_user = $this->admin_model->get_single_record('subscription','user_id',$session_data['id'],'Y');   
        if(count($subscrib_user) > 0){
            $data['invoice_list'] = $this->get_invoice_list(); 
            $upcoming =  \Stripe\Invoice::upcoming(array("customer" => $subscrib_user['stripe_customer_id']));
            $data['upcoming'] = $upcoming->jsonSerialize();        
        } else {
            $data['upcoming'] = '';
            $data['invoice_list'] = '';
        }        
        $this->layout->view('admin/subscription',$data);
    }

    public function support(){
        $this->is_login();
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Support  ');       
        $this->layout->view('admin/support');
    }


    public function billing_history(){
        $this->is_login();
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Billing History');       
        $this->layout->view('admin/billing_history');
    }

    public function amazon_accounts() {
        $this->is_login();
        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Amazon Accounts'); 

        $data['account_detail'] = $this->admin_model->get_allrecords('amazon_mws_accounts');
        $data['marketplace_list'] = $this->admin_model->marketplaceid_list(); 

        $this->layout->view('admin/amazon_accounts_list',$data);
    }

    function amazon_account_edit($id='') {       
        
        $this->is_login();

        $this->layout->layout_view = 'layouts/adminlayout.php';
        $this->layout->title('Jejo Pages - Edit Amazon Account');        
        $data['amazon_accounts'] = $this->admin_model->getUserById('amazon_mws_accounts',$id);
         if($this->input->post('submit')){ 


            $rslt = array(                
                'serviceurl' => $this->input->post('serviceurl'),
                'seller_id' => $this->input->post('seller_id'),              
                'authtoken' => $this->input->post('authtoken'),
                'status' => $this->input->post('status')
            );

      
            $this->admin_model->updateUser('amazon_mws_accounts',$rslt,$id);    
            $this->session->set_flashdata("message","<font class='success'>Amazon Account successfully Updated.</font>");

            redirect('admin/amazon_accounts');            
        }
                
        $this->layout->view('admin/amazon_account_edit', $data);
    }


    function user_log($action=''){
        $session_data = $this->session->userdata('logged_in');
        $log['user'] = $session_data['display_name'];
        $log['ip'] = $_SERVER['REMOTE_ADDR'];
        $log['user_id'] =  $session_data['id'];
        $log['action'] = $action;
        $log['date_time'] = date('Y-m-d h:i:s');
       
        $logdata = $this->admin_model->addLog($log);
    }
    
    function ajax_user_log(){
        $action = $_POST['action'];
        $session_data = $this->session->userdata('logged_in');
        $log['user'] = $session_data['display_name'];
        $log['ip'] = $_SERVER['REMOTE_ADDR'];
        $log['user_id'] =  $session_data['id'];
        $log['action'] = "Amazon authentication ".$action;
        $log['date_time'] = date('Y-m-d h:i:s');   
        $logdata = $this->admin_model->addLog($log);
    }

    public function create_subscription() {

        try {

            $planName = "basic";
            $planAmount = 50;

            $session_data = $this->session->userdata('logged_in');
            $customer = \Stripe\Customer::create(array(
            'email' =>  $_POST['stripeEmail'],
            'source'  => $_POST['stripeToken'],
            'plan' => $planName
            ));  

            $subsc = $customer->jsonSerialize();          

            $result['user_id'] = $session_data['id'];
            $result['stripe_customer_id'] = $customer['id'];  
            $result['stripe_subscription_id'] = $subsc['subscriptions']['data'][0]['id'];  
            $result['email'] = $_POST['stripeEmail'];  
            $result['plan_name'] = $planName;  
            $result['plan_amount'] = $planAmount;   

            $this->admin_model->addUser('subscription',$result);

             $this->session->set_flashdata("message","<font class='success'>Plan activated successfully.</font>");
               redirect('admin/subscription');
            exit;

        } catch(Exception $e) {
            $this->session->set_flashdata("error","Plan activation unsuccessful. Please try again and contact us at support@jejo.digital if the problem persists.");
            redirect('admin/subscription');
        }
        
    }

    public function get_invoice_list(){
        
        $session_data = $this->session->userdata('logged_in');
        $subscrib_user = $this->admin_model->get_single_record('subscription','user_id',$session_data['id'],'Y');    
        $invoice_list = \Stripe\Invoice::all(array('customer' => $subscrib_user['stripe_customer_id']));
        $result = $invoice_list->jsonSerialize();  
        return $result; 

    }   


    public function generate_invoice($invoice_id='') {
                      
        $session_data = $this->session->userdata('logged_in');

        $rcarray = \Stripe\Invoice::retrieve($invoice_id);

        $data['invoice_info'] = $rcarray->jsonSerialize();

        $subscrib_user = $this->admin_model->get_single_record('subscription','user_id',$session_data['id'],'Y'); 
    
        $data['email'] = $subscrib_user['email']; 
      
        $this->dompdf->load_html($this->load->view('admin/invoice_view',$data,true));
        $this->dompdf->render();
        $this->dompdf->stream(date('Y-m-d')."_invoice.pdf");
        return;
    }

    public function subscription_cancel() {
          
        try {
       
            $session_data = $this->session->userdata('logged_in');
            $subscrib_user = $this->admin_model->get_single_record('subscription','user_id',$session_data['id'],'Y');

            $subscription = \Stripe\Subscription::retrieve($subscrib_user['stripe_subscription_id']); 
            $subscription->cancel();
            
            $data['status'] = 0;
            $this->admin_model->update($data,'subscription','user_id',$session_data['id']);

            $this->session->set_flashdata("message","<font class='success'>Your subscription cancel successfully! </font>");
            redirect('admin/subscription');
            exit;

       } catch(Exception $e) {
 
            $this->session->set_flashdata("error","Your subscription cancel not successfully!");
            redirect('admin/subscription');
           
       }        

    }
    

    public function get_timezone($offset){
        // Calculate seconds from offset
        list($hours, $minutes) = explode(':', $offset);
        $seconds = $hours * 60 * 60 + $minutes * 60;
        // Get timezone name from seconds
        $tz = timezone_name_from_abbr('', $seconds, 1);
         
        if($tz === false) $tz = timezone_name_from_abbr('', $seconds, 0);

        return $tz;
    }
}
?>
