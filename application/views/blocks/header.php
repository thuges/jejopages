<!-- begin #header -->
<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="#" class="navbar-brand"><span class="navbar-logo"></span>Jejo Pages</a>
            <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
        </div>
        <!-- end mobile sidebar expand / collapse button -->
        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                 <?php
                $email = $this->session->userdata['logged_in']['email'];
                $default = base_url().'assets/admin/images/default.png';
                $size = 40;
                $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default );
                ?>
                <img src="<?php echo $grav_url; ?>" alt="" /> 
                <span class="hidden-xs"><?php echo $this->session->userdata['logged_in']['display_name']; ?></span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInLeft">
                    <li class="arrow"></li>
                    <li><a href="<?php echo base_url().'admin/profile_edit'; ?>">Account</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/amazon-accounts">Amazon</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/subscription">Subscription</a></li>
                    <li><a href="<?php echo base_url(); ?>admin/support">Support</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url(); ?>admin/billing-history">Billing History</a></li>
                    <li class="divider"></li>                    
                    <li><a href="<?php echo base_url()?>admin/logout">Log Out</a></li>
                </ul>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
<!-- end #header -->
