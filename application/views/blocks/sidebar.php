<!-- begin #sidebar -->
<?php 
    $current_method = $this->uri->segment('1').'/'.$this->uri->segment('2'); 
?>
<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <div class="image">
                <?php
                $email = $this->session->userdata['logged_in']['email'];
                $default = base_url().'assets/admin/images/default.png';
                $size = 40;
                $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default );
                ?>
 
                    <a href="javascript:;"><img src="<?php echo $grav_url; ?>" alt="" /></a>
                </div>
                <div class="info">
                    <?php echo $this->session->userdata['logged_in']['display_name']; ?>
                    <small><?php echo $this->session->userdata['logged_in']['role'] ?></small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigation</li>
            <li class="has-sub <?php echo ($current_method == "dashboard/") ? "active" : ""; ?>">
                <a href="javascript:;">
                <i class="fa fa-laptop"></i>                 
                <span>Dashboard</span>
                </a>         
            </li>
            <li class="has-sub <?php echo ($current_method == "admin/timezone" || $current_method == "admin/amazon-accounts" || $current_method == "admin/profile_edit" || $current_method == "admin/subscription") ? "active" : ""; ?>">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-cogs"></i>
                    <span>Settings</span>
                </a>
                <ul class="sub-menu">
                    <li class="<?php echo ($current_method == "admin/timezone") ? "active" : ""; ?>">
                        <a href="<?php echo base_url(); ?>admin/timezone">
                            Timezone
                        </a>
                    </li>      
                    
                    <li class="<?php echo ($current_method == "admin/profile_edit") ? "active" : ""; ?>">
                        <a href="<?php echo base_url(); ?>admin/profile_edit">
                        <span>Account</span>
                        </a>                
                    </li>
          
                    <li class="<?php echo ($current_method == "admin/amazon-accounts") ? "active" : ""; ?>">
                        <a href="<?php echo base_url(); ?>admin/amazon-accounts">
                        <span>Amazon</span>
                        </a>                
                    </li> 
                  
                    <li class="<?php echo ($current_method == "admin/subscription") ? "active" : ""; ?>">
                        <a href="<?php echo base_url(); ?>admin/subscription">
                        <span>Subscription</span>
                        </a>                
                    </li>

                </ul>
            </li>
            <?php
             $session_data = $this->session->userdata('logged_in');          
             if($session_data['role'] == 'Administrator'){
            ?>   
               <li class="has-sub <?php echo ($current_method == "admin/user_list" || $current_method == "admin/user_edit" || $current_method == "admin/adduser" || $current_method == "admin/logs") ? "active" : ""; ?>">
                    <a href="javascript:;">                            
                    <b class="caret pull-right"></b>
                    <i class="fa fa-users"></i> 
                    <span>User Management</span>
                    </a>
                    <ul class="sub-menu">
                        <li class="<?php echo ($current_method == "admin/user_list" || $current_method == "admin/user_edit" || $current_method == "admin/adduser") ? "active" : ""; ?>"><a href="<?php echo base_url(); ?>admin/user_list">User Accounts</a></li>
                        <li class="<?php echo ($current_method == "admin/logs") ? "active" : ""; ?>" ><a href="<?php echo base_url(); ?>admin/logs">User Access Log</a></li>
                    </ul>
                </li>  
             <?php } ?>
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->