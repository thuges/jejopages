<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">Manage User</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">User Management<small></small></h1>
    <!-- end page-header -->
    <?php
        $message = $this->session->flashdata('message');
        if($message){
        ?>
    <div class="alert alert-success fade in m-b-15">
        <strong><i class="fa fa-check"></i></strong>
        <?php echo $this->session->flashdata('message'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php }
        $this->session->unset_userdata('message'); 
        $error = $this->session->flashdata('error');
               if($error){
        ?>
    <div class="alert alert-danger fade in m-b-15">
        <strong> :( </strong>
        <?php echo $this->session->flashdata('error'); ?>
        <span class="close fa fa-2x fa-times-circle" data-dismiss="alert"></span>
    </div>
    <?php } 
        $this->session->unset_userdata('error');
        ?>
    <!-- begin row -->
    <a class="btn btn-danger m-r-5 m-b-5" href="<?php echo base_url()."admin/adduser"?>">
    <i class="fa fa-plus"></i>
        Add User
    </a>
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">User</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive user_table">
                        <table id="user-data-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>                                     
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  if(isset($user_detail)){ 
                                    $idx = 1; foreach ($user_detail as $key => $value) { ?>
                                <tr>
                                    <td><?php echo $idx ++; ?></td>
                                    <td><?php echo $value->display_name ?></td>
                                    <td><?php echo $value->email; ?></td>                                     
                                     <td><?php if($value->role == 2){ echo 'Administrator'; } else { echo 'User'; }  ?></td>
                                    <td> 
                                        <a href="<?php echo base_url().'admin/user_edit/'.$value->id; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit" class="btn btn-default btn-icon btn-circle btn-lg edituser" id="edituser" ><i class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete" class="btn btn-danger btn-icon btn-circle btn-lg deleteuser action_link" id="deleteuser" data-id="<?php echo $value->id; ?>"><i class="fa fa-trash-o"></i></a>                              
                                    </td>
                                </tr>
                                <?php } }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->