<!-- begin #content -->
<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">Manage Amazon Accounts</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header"> Amazon Accounts<small></small></h1>
    <!-- end page-header -->
    <?php
        $message = $this->session->flashdata('message');
        if($message){
        ?>
    <div class="alert alert-success fade in m-b-15">
        <strong><i class="fa fa-check"></i></strong>
        <?php echo $this->session->flashdata('message'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php }
        $this->session->unset_userdata('message'); 
        $error = $this->session->flashdata('error');
               if($error){
        ?>
    <div class="alert alert-danger fade in m-b-15">
        <strong> :( </strong>
        <?php echo $this->session->flashdata('error'); ?>
        <span class="close fa fa-2x fa-times-circle" data-dismiss="alert"></span>
    </div>
    <?php } 
        $this->session->unset_userdata('error');
        ?>
    <!-- begin row -->
    <a class="btn btn-danger m-r-5 m-b-5" href="<?php echo base_url()."admin/amazonmws"?>">
    ADD AN AMAZON ACCOUNT</a>
    <div class="row">
        <!-- begin col-10 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Amazon Accounts List</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive amazon_table">
                        <table id="amazonmws-data-table" class="table text-center  table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">MARKETPLACE</th>
                                    <th class="text-center">NAME</th>
                                    <th class="text-center">SELLER ID</th>
                                    <th class="text-center">MWS STATUS</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  if(isset($account_detail)){ 
                                    $idx = 1; foreach ($account_detail as $key => $value) { ?>
                                <tr>
                                    <td>
                                        <?php
                                            if($value->serviceurl == 'https://mws.amazonservices.com/'){ 
                                        ?>
                                        <img src="<?php  echo base_url().'assets/admin/images/flags/us.png'; ?>" alt=""> 
                                        <?php  } elseif($value->serviceurl == 'https://mws-eu.amazonservices.com/') { ?>
                                        <img src="<?php  echo base_url().'assets/admin/images/flags/eu.png'; ?>" alt="">
                                        <?php  } elseif($value->serviceurl == 'https://mws.amazonservices.in/') { ?>
                                        <img src="<?php  echo base_url().'assets/admin/images/flags/in.png'; ?>" alt="">
                                        <?php  } elseif($value->serviceurl == 'https://mws.amazonservices.com.cn') {  ?>
                                        <img src="<?php  echo base_url().'assets/admin/images/flags/cn.png'; ?>" alt="">
                                        <?php  } elseif($value->serviceurl == 'https://mws.amazonservices.jp') { ?>
                                        <img src="<?php  echo base_url().'assets/admin/images/flags/jp.png'; ?>" alt="">
                                        <?php  } else { echo '';  }?>
                                    </td>
                                    <td>
                                        <?php
                                            if($value->serviceurl == 'https://mws.amazonservices.com/'){ 
                                        echo 'SELLER CENTRAL USA';
                                        } elseif($value->serviceurl == 'https://mws-eu.amazonservices.com/') { 
                                            echo 'SELLER CENTRAL EU';
                                         } elseif($value->serviceurl == 'https://mws.amazonservices.in/') {
                                            echo 'SELLER CENTRAL IN';
                                         } elseif($value->serviceurl == 'https://mws.amazonservices.com.cn') {   
                                             echo 'SELLER CENTRAL CN';
                                         } elseif($value->serviceurl == 'https://mws.amazonservices.jp') { echo 'SELLER CENTRAL JP';
                                         } else { echo '';  }?>
                                    </td>
                                    <td><?php echo $value->seller_id; ?></td>
                                    <td><?php if($value->status == 1){ ?>
                                        <label class="btn btn-success">ACTIVE</label>
                                        <?php } else { ?>
                                        <label class="btn btn-warning">DEACTIVE</label>
                                        <?php } ?>
                                    </td>
                                    <td> 
                                        <a href="<?php echo base_url().'admin/amazon_account_edit/'.$value->id; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit" class="btn btn-default btn-icon btn-circle btn-lg edituser" id="edituser" ><i class="fa fa-edit"></i></a>
                                        <a href="javascript:void(0);" data-toggle="tooltip" data-placement="bottom" title="Delete" class="btn btn-danger btn-icon btn-circle btn-lg delete_amazonmws action_link" id="delete_amazonmws" data-id="<?php echo $value->id; ?>"><i class="fa fa-trash-o"></i></a>                              
                                    </td>
                                </tr>
                                <?php } }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-10 -->
    </div>
    <!-- end row -->
</div>
<!-- end #content -->