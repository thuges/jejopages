<!-- begin #content -->
<div id="content" class="content">
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Home</a></li>
    <li class="active">Edit Amazon Account</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Edit Amazon Account<small></small></h1>
<!-- end page-header -->
<?php
        $message = $this->session->flashdata('message');
        $error = $this->session->flashdata('error');
        if($message){
    ?>         
    <div class="alert alert-success fade in m-b-15">
        <strong><i class="fa fa-check"></i></strong>
        <?php echo $this->session->flashdata('message'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php } if($error) { ?>    
    <div class="alert alert-danger fade in m-b-15">
        <strong><i class="fa fa-close"></i></strong>
        <?php echo $this->session->flashdata('error'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php } ?>
 
<div class="row">
    <!-- begin col-6 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">Amazon Account</h4>
            </div>
            <form action="<?php echo base_url() ?>admin/amazon_account_edit/<?php echo isset($amazon_accounts['id']) ? $amazon_accounts['id'] : ''; ?>" method="post" data-parsley-validate="true"  name="demo-form">
                <div class="panel-body">
                     <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="serviceurl">Service URL</label>
                        <div class="col-md-9">
                              <select name="serviceurl" class="form-control">
                                <option <?php if($amazon_accounts['serviceurl'] == 'https://mws.amazonservices.com/') { echo 'selected'; } else { echo ''; } ?> selected value="https://mws.amazonservices.com/">North America (NA)</option> 
                                <option <?php if($amazon_accounts['serviceurl'] == 'https://mws.amazonservices.com/' 
                                AND $amazon_accounts['marketplaceid'] == 'A2Q3Y263D00KWC' ) { echo 'selected'; } else { echo ''; } ?> value="https://mws.amazonservices.com/">Brazil (BR)</option> 
                                <option <?php if($amazon_accounts['serviceurl'] == 'https://mws-eu.amazonservices.com/') { echo 'selected'; } else { echo ''; } ?> value="https://mws-eu.amazonservices.com/">Europe (EU)</option> 
                                <option <?php if($amazon_accounts['serviceurl'] == 'https://mws.amazonservices.in/') { echo 'selected'; } else { echo ''; } ?> value="https://mws.amazonservices.in/">India (IN)</option> 
                                <option <?php if($amazon_accounts['serviceurl'] == 'https://mws.amazonservices.com.cn/') { echo 'selected'; } else { echo ''; } ?> value="https://mws.amazonservices.com.cn/">China (CN)</option> 
                                <option <?php if($amazon_accounts['serviceurl'] == 'https://mws.amazonservices.jp/') { echo 'selected'; } else { echo ''; } ?> value="https://mws.amazonservices.jp/">Japan (JP)</option> 
                           </select>
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="seller_id">Seller Id </label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" id="seller_id" name="seller_id" placeholder="Enter Seller Id" value="<?php echo isset($amazon_accounts['seller_id']) ? $amazon_accounts['seller_id'] : ''; ?>" data-parsley-required="true" required />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="authtoken">MWS Auth Token</label>
                        <div class="col-md-9">
                             <input class="form-control" type="text" id="authtoken" name="authtoken" placeholder="Enter Access Key ID" value="<?php echo isset($amazon_accounts['authtoken']) ? $amazon_accounts['authtoken'] : ''; ?>" data-parsley-required="true" required />
                        </div>
                    </div>                   
                     <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="status">Status</label>
                        <div class="col-md-9">
                            <select class="combobox" name="status"  >
                                <option value="">Select Status</option>
                                <option value="1" <?php if( $amazon_accounts['status'] == 1) { echo 'selected'; } ?> >Active</option>
                                <option value="0" <?php if( $amazon_accounts['status'] == 0) { echo 'selected'; } ?> >Deactive</option>
                            </select>                            
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                            <input type="submit" name="submit" value="save" class="btn btn-danger pull-right">                                        
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-6 -->
</div>
<style type="text/css">
    
  .input-group-addon {
  -webkit-order: 2;
  order: 2;}  
</style>

