<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">Amazon MWS</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Amazon MWS Authentication</h1>

    <!-- Display Success Message for test connection and stay in same tab-->
    <?php
        $message = $this->session->flashdata('message');
        $error = $this->session->flashdata('error');
        if($message){
    ?>
        <script>
            setTimeout(function(){
                $('#step4').trigger('click');
            }, 1000);
        </script>
    <div class="alert alert-success fade in m-b-15">
        <strong><i class="fa fa-check"></i></strong>
        <?php echo $this->session->flashdata('message'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php } if($error) { ?>
    <script>
        setTimeout(function(){
            $("#step4").trigger('click');
        }, 1000);
    </script>
    <div class="alert alert-danger fade in m-b-15">
        <strong><i class="fa fa-close"></i></strong>
        <?php echo $this->session->flashdata('error'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php } ?>
    <div class="row">
        <!-- begin col-6 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Amazon Marketplace Web Services authentication</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                                <form action="<?php echo base_url(); ?>admin/check_amazon_auth" method="post" name="form-wizard">
                                <div id="wizard">
                                    <ol>
                                        <li id="step1">Open MWS</li>
                                        <li id="step2">Login</li>
                                        <li id="step3">Developer Access</li>
                                        <li id="step4">Access Keys</li>                                         
                                    </ol>
                                    <!-- begin wizard step-1 -->
                                    <div >
                                        <fieldset class="text-center">

                                            <!-- begin row -->
                                            <div class="row">
                                                <!-- begin col-12 -->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                    <br />
                                                         <p>Your web application needs to access your Amazon account through MWS (Amazon Marketing Web Services) in order for everything to work correctly.</p>
                                                         <p>This wizard will guide you through the process of setting up this connection.</p>
                                                         <p>Please click this button to open the MWS Authentication page and we’ll explain what to do next in Step 2.
                                                         <p class="m-t-20"><a  id="amazonbtn" target="_blank" class="btn btn-primary btn-lg" href="https://sellercentral.amazon.com/gp/mws/registration/register.html?signInPageDisplayed=1&developerName=JejoDigital&devMWSAccountId=2281-3503-7084">Open MWS authentication page</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end row -->
                                        </fieldset>
                                    </div>
                                    <!-- end wizard step-1 -->
                                    <!-- begin wizard step-2 -->
                                    <div>
                                        <fieldset class="text-center">
                                            <legend class="pull-left width-full">Step 2: Login with your Amazon Seller Central account.</legend>
                                            <!-- begin row -->
                                            <div class="row">
                                                <!-- begin col-6 -->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <p>If asked to sign in to your account, please sign in with your Amazon Seller Central account for the marketplace you're authenticating.</p>
                                                        <p><em>If you're already signed in you can skip this step.</em></p>
                                                        <p><h5><a class="btn btn-primary btn-lg m-t-10" id="nextstep" href="#">proceed to step 3</a></h5></p>
                                                        <img class="m-t-20" src="<?php echo base_url() ?>assets/admin/images/sellercentrallogin.png" height="500" width="700" alt="MWS Authentication step 2" />
                                                    </div>
                                                </div>
                                                <!-- end col-6 -->
                                            </div>
                                            <!-- end row -->
                                        </fieldset>
                                    </div>
                                    <!-- end wizard step-2 -->
                                    <!-- begin wizard step-3 -->
                                    <div>
                                        <fieldset class="text-center">
                                            <legend class="pull-left width-full">Step 3: Configure developer access for this web application.</legend>
                                            <!-- begin row -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                         <p>Our developer account information should already be configured. Please verify it and <strong>click 'Next'</strong></p>
                                                         <p><strong>Check the checkbox on the next page</strong> and <strong>click 'Next'</strong><strong>Next</strong>'</p>
                                                        <label>Developer's Name: <strong>JejoDigital</strong></label><br>
                                                        <label>Developer Account Number: <strong>2281-3503-7084</strong></label>
                                                        <p><h5><a class="btn btn-primary btn-lg m-t-10" id="nextStepAccess" href="#">proceed to step 4</a></h5></p>
                                                        <hr>
                                                        <img class="m-t-20" src="<?php echo base_url() ?>assets/admin/images/configure.png" height="500" width="700" alt="MWS Authentication step 3" />

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end row -->
                                        </fieldset>
                                    </div>
                                    <!-- end wizard step-3 -->
                                    <!-- begin wizard step-4 -->
                                    <div class="wizard-step-2 text-center">
                                        <legend class="pull-left width-full">Step 4: Enter Access Keys</legend>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p>Please enter the <strong>Seller account identifiers</strong> here:</p>
                                                    <div class="col-sm-4">
                                                        <label>Amazon Marketplace</label>
                                                        <div class="controls">
                                                            <select name="serviceurl" class="form-control">
                                                                <option <?php if($this->session->userdata('serviceurl') == 'https://mws.amazonservices.com/') { echo 'selected'; } else { echo ''; } ?> selected value="https://mws.amazonservices.com/">North America (NA)</option>
                                                                <option <?php if($this->session->userdata('serviceurl') == 'https://mws-eu.amazonservices.com/') { echo 'selected'; } else { echo ''; } ?> value="https://mws-eu.amazonservices.com/">Europe (EU)</option>
                                                                <option <?php if($this->session->userdata('serviceurl') == 'https://mws.amazonservices.in/') { echo 'selected'; } else { echo ''; } ?> value="https://mws.amazonservices.in/">India (IN)</option>
                                                                <option <?php if($this->session->userdata('serviceurl') == 'https://mws.amazonservices.com.cn/') { echo 'selected'; } else { echo ''; } ?> value="https://mws.amazonservices.com.cn/">China (CN)</option>
                                                                <option <?php if($this->session->userdata('serviceurl') == 'https://mws.amazonservices.jp/') { echo 'selected'; } else { echo ''; } ?> value="https://mws.amazonservices.jp/">Japan (JP)</option>
                                                           </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label>Seller ID</label>
                                                        <div class="controls">
                                                        <input type="text"  name="sellerid" value="<?php echo $this->session->userdata('seller_id'); ?>"  class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-4 form-group">
                                                        <label>MWS Auth Token</label>
                                                        <div class="controls">
                                                        <input type="text" name="authtoken" value="<?php echo $this->session->userdata('authtoken'); ?>" class="form-control">
                                                        </div>
                                                    </div>
                                                    <p><h5><input type="submit" name="submit" value="submit and test connection" class="btn btn-primary btn-lg m-t-10"></h5></p>

                                                </div>

                                                <hr>

                                                <img class="m-t-20" src="<?php echo base_url() ?>assets/admin/images/copy.png" height="500" width="700" style="margin-top: 30px;" alt="MWS Authentication step 4" />
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end wizard step-4 -->
                                   
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->