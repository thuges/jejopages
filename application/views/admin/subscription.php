<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">Subscription</li>
    </ol>
    <!-- end breadcrumb -->
    <h1 class="page-header">Subscription <span></span></h1>
    <?php
        $message = $this->session->flashdata('message');
        if($message){
        ?>
    <div class="alert alert-success fade in m-b-15">
        <strong><i class="fa fa-check"></i></strong>
        <?php echo $this->session->flashdata('message'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php }
        $this->session->unset_userdata('message'); 
        $error = $this->session->flashdata('error');
        if($error){
        ?>
    <div class="alert alert-danger fade in m-b-15">
        <strong> :( </strong>
        <?php echo $this->session->flashdata('error'); ?>
        <span class="close fa fa-2x fa-times-circle" data-dismiss="alert"></span>
    </div>
    <?php } 
        $this->session->unset_userdata('error');
        $session_data = $this->session->userdata('logged_in'); 
        ?>
    <div class="row">
    
        <?php if(isset($upcoming['lines']['data'][0]['plan']['id']) AND  $upcoming['lines']['data'][0]['plan']['id'] !='' ){ ?>
        <div class="col-md-3 col-sm-6"> 
            <div class="widget widget-stats bg-blue">
                <div class="stats-icon stats-icon-lg"><i class="fa fa-user"></i></div>
                <div class="stats-title">Your plan</div>
                <div class="stats-number"><?php echo $upcoming['lines']['data'][0]['plan']['id']; ?></div>
                <div class="stats-progress progress">
                    <div class="progress-bar" style="width:0%;"></div>
                </div>
                <div class="stats-desc">renews on <?php echo strftime("%m/%d/%Y",$upcoming['date']); ?></div>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="row">

        <div class="col-sm-4">   
            <div class="panel panel-info" data-sortable-id="ui-widget-16">
                <div class="panel-heading">                   
                    <h4 class="panel-title">Plan 1</h4>
                </div>
                <div class="panel-body bg-aqua text-white">      
                        <form class="text-center" action="<?php echo base_url(); ?>admin/create_subscription" method="POST">
                            <script
                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                data-key="pk_test_TSfY69U8Nf6KDDk6xrZdGL2Y"
                                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                data-name="Jejo pages"
                                data-description="Subscription 1 Month for Jejo Pages"
                                data-amount="5000.00"
                                data-label="Subscribe!">
                            </script>
                        </form> 
                    
                </div>
            </div>
        </div>

        <div class="col-sm-4"> 
           <div class="panel panel-info" data-sortable-id="ui-widget-16">
                <div class="panel-heading">                   
                    <h4 class="panel-title">Plan 2</h4>
                </div>
                <div class="panel-body bg-aqua text-white">  
                        <form class="text-center" action="<?php echo base_url(); ?>admin/create_subscription" method="POST">
                            <script
                                src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                data-key="pk_test_TSfY69U8Nf6KDDk6xrZdGL2Y"
                                data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                data-name="Jejo pages"
                                data-description="Subscription 1 Month for Jejo Pages"
                                data-amount="5000.00"
                                data-label="Subscribe!">
                            </script>
                        </form>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="panel panel-info" data-sortable-id="ui-widget-16">
                <div class="panel-heading">                   
                    <h4 class="panel-title">Plan 3</h4>
                </div>
                <div class="panel-body bg-aqua text-white">                      
                   <form class="text-center" action="<?php echo base_url(); ?>admin/create_subscription" method="POST">
                        <script
                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                            data-key="pk_test_TSfY69U8Nf6KDDk6xrZdGL2Y"
                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                            data-name="Jejo pages"
                            data-description="Subscription 1 Month for Jejo Pages"
                            data-amount="5000.00"
                            data-label="Subscribe!">
                        </script>
                    </form>
                </div>
            </div>
        </div> 
       </div>
        <div class="row">
             <div class="col-sm-12 form-group">
                  <form action="<?php echo base_url(); ?>admin/subscription_cancel" method="POST">
                    <button class="btn btn-danger">Cancel Subscription</button>
                </form>
            </div>
            <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Invoices</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive invoice_table">
                        <table id="invoice_table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Invoice number</th>
                                    <th>Amount</th>                                     
                                    <th>Status</th>
                                    <th>Download Invoice</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(isset($invoice_list['data'])){ 
                                for($i=0; $i < count($invoice_list['data']);$i++){ ?>
                                    <tr>
                                        <td><?php echo strftime("%m/%d/%Y",$invoice_list['data'][$i]['date']); ?></td>        
                                        <td><?php echo $invoice_list['data'][$i]['id']; ?></td>      
                                        <td>$<?php echo number_format(($invoice_list['data'][$i]['total'] /100), 2, '.', ' '); ?></td>    
                                        <td>
                                          <?php if($invoice_list['data'][$i]['paid'] == 1) { ?>
                                                <label class="label label-success">Paid</label>
                                          <?php } else { ?>  
                                                <label class="label label-danger">Unpaid</label>
                                          <?php } ?>
                                        </td>
                                        <td><a class="btn btn-default" href="<?php echo base_url().'admin/generate_invoice/'.$invoice_list['data'][$i]['id']; ?>">Download</a></td> 
                                    </tr>
                                <?php } }  ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end panel -->
        </div>   
        
    </div>
</div>
<!-- /.content-wrapper -->