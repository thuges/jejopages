<div id="content" class="content">
    <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Home</a></li>
        <li class="active">Timezone</li>
    </ol>
    <!-- end breadcrumb -->
    <!-- begin page-header -->
    <h1 class="page-header">Timezone</h1>
    <?php 
        $message = $this->session->flashdata('message');
        if($message){
        ?>
     <div class="alert alert-success fade in m-b-15">
        <strong><i class="fa fa-check"></i></strong>
        <?php echo $this->session->flashdata('message'); ?>
        <span data-dismiss="alert" class="close fa fa-2x fa-times-circle"></span>
    </div>
    <?php } ?>
    <div class="row">
        <!-- begin col-6 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-validation-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Timezone</h4>
                </div>                
                <form action="<?php echo base_url(); ?>admin/update_timezone" method="post"  >	
                    <div class="panel-body">
                    	<input type="hidden" name="timezone_name" id="timezone_name" value="">
                        <div class="form-group clearfix">
                            <label class="control-label col-md-3" for="timezon">Select Timezone</label>
                            <div class="col-md-9">
                                <select name="timezone" id="timezone" data-style="btn-white" class="form-control selectpicker" data-size="10" data-live-search="true" >
									<option <?php if($timezone[0]->timezone == "(GMT-12:00) International Date Line West"){ echo 'selected'; } ?> data-timezone="Pacific/Wake" timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="(GMT-12:00) International Date Line West">(GMT-12:00) International Date Line West</option>
									<option <?php if($timezone[0]->timezone == "(GMT-11:00) Midway Island, Samoa"){ echo 'selected'; } ?> data-timezone="US/Samoa" timeZoneId="2" gmtAdjustment="GMT-11:00" useDaylightTime="0" value="(GMT-11:00) Midway Island, Samoa">(GMT-11:00) Midway Island, Samoa</option>
									<option <?php if($timezone[0]->timezone == "(GMT-10:00) Hawaii"){ echo 'selected'; } ?> data-timezone="US/Hawaii" timeZoneId="3" gmtAdjustment="GMT-10:00" useDaylightTime="0" value="(GMT-10:00) Hawaii">(GMT-10:00) Hawaii</option>
									<option <?php if($timezone[0]->timezone == "(GMT-09:00) Alaska"){ echo 'selected'; } ?> data-timezone="US/Alaska" timeZoneId="4" gmtAdjustment="GMT-09:00" useDaylightTime="1" value="(GMT-09:00) Alaska">(GMT-09:00) Alaska</option>
									<option <?php if($timezone[0]->timezone == "(GMT-08:00) Pacific Time (US & Canada)"){ echo 'selected'; } ?> data-timezone="US/Pacific" timeZoneId="5" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="(GMT-08:00) Pacific Time (US & Canada)">(GMT-08:00) Pacific Time (US & Canada)</option>
									<option <?php if($timezone[0]->timezone == "(GMT-08:00) Tijuana, Baja California"){ echo 'selected'; } ?> data-timezone="America/Tijuana" timeZoneId="6" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="(GMT-08:00) Tijuana, Baja California">(GMT-08:00) Tijuana, Baja California</option>
									<option <?php if($timezone[0]->timezone == "(GMT-07:00) Arizona"){ echo 'selected'; } ?> data-timezone="US/Arizona" timeZoneId="7" gmtAdjustment="GMT-07:00" useDaylightTime="0" value="(GMT-07:00) Arizona">(GMT-07:00) Arizona</option>
									<option <?php if($timezone[0]->timezone == "(GMT-07:00) Chihuahua, La Paz, Mazatlan"){ echo 'selected'; } ?> data-timezone="America/Mazatlan" timeZoneId="8" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="(GMT-07:00) Chihuahua, La Paz, Mazatlan">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
									<option <?php if($timezone[0]->timezone == "(GMT-07:00) Mountain Time (US & Canada)"){ echo 'selected'; } ?> data-timezone="US/Mountain" timeZoneId="9" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="(GMT-07:00) Mountain Time (US & Canada)">(GMT-07:00) Mountain Time (US & Canada)</option>
									<option <?php if($timezone[0]->timezone == "(GMT-06:00) Central America"){ echo 'selected'; } ?> data-timezone="US/Central" timeZoneId="10" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="(GMT-06:00) Central America">(GMT-06:00) Central America</option>
									<option <?php if($timezone[0]->timezone == "(GMT-06:00) Central Time (US & Canada)"){ echo 'selected'; } ?> data-timezone="US/Central" timeZoneId="11" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="(GMT-06:00) Central Time (US & Canada)">(GMT-06:00) Central Time (US & Canada)</option>
									<option <?php if($timezone[0]->timezone == "(GMT-06:00) Guadalajara, Mexico City, Monterrey"){ echo 'selected'; } ?> data-timezone="America/Mexico_City" timeZoneId="12" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="(GMT-06:00) Guadalajara, Mexico City, Monterrey">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
									<option <?php if($timezone[0]->timezone == "(GMT-06:00) Saskatchewan"){ echo 'selected'; } ?> data-timezone="Canada/Saskatchewan" timeZoneId="13" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="(GMT-06:00) Saskatchewan">(GMT-06:00) Saskatchewan</option>
									<option <?php if($timezone[0]->timezone == "(GMT-05:00) Bogota, Lima, Quito, Rio Branco"){ echo 'selected'; } ?> data-timezone="America/Bogota" timeZoneId="14" gmtAdjustment="GMT-05:00" useDaylightTime="0" value="(GMT-05:00) Bogota, Lima, Quito, Rio Branco">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
									<option <?php if($timezone[0]->timezone == "(GMT-05:00) Eastern Time (US & Canada)"){ echo 'selected'; } ?> data-timezone="US/Eastern" timeZoneId="15" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="(GMT-05:00) Eastern Time (US & Canada)">(GMT-05:00) Eastern Time (US & Canada)</option>
									<option <?php if($timezone[0]->timezone == "(GMT-05:00) Indiana (East)"){ echo 'selected'; } ?> data-timezone="US/East-Indiana" timeZoneId="16" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="(GMT-05:00) Indiana (East)">(GMT-05:00) Indiana (East)</option>
									<option <?php if($timezone[0]->timezone == "(GMT-04:00) Atlantic Time (Canada)"){ echo 'selected'; } ?> data-timezone="Canada/Atlantic" timeZoneId="17" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="(GMT-04:00) Atlantic Time (Canada)">(GMT-04:00) Atlantic Time (Canada)</option>
									<option <?php if($timezone[0]->timezone == "(GMT-04:00) Caracas, La Paz"){ echo 'selected'; } ?> data-timezone="America/La_Paz" timeZoneId="18" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="(GMT-04:00) Caracas, La Paz">(GMT-04:00) Caracas, La Paz</option>
									<option <?php if($timezone[0]->timezone == "(GMT-04:00) Manaus"){ echo 'selected'; } ?> data-timezone="America/Manaus" timeZoneId="19" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="(GMT-04:00) Manaus">(GMT-04:00) Manaus</option>
									<option <?php if($timezone[0]->timezone == "(GMT-04:00) Santiago"){ echo 'selected'; } ?> data-timezone="America/Santiago" timeZoneId="20" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="(GMT-04:00) Santiago">(GMT-04:00) Santiago</option>
									<option <?php if($timezone[0]->timezone == "(GMT-03:30) Newfoundland"){ echo 'selected'; } ?> data-timezone="Canada/Newfoundland" timeZoneId="21" gmtAdjustment="GMT-03:30" useDaylightTime="1" value="(GMT-03:30) Newfoundland">(GMT-03:30) Newfoundland</option>
									<option <?php if($timezone[0]->timezone == "(GMT-03:00) Brasilia"){ echo 'selected'; } ?> data-timezone="America/Maceio" timeZoneId="22" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="(GMT-03:00) Brasilia">(GMT-03:00) Brasilia</option>
									<option <?php if($timezone[0]->timezone == "(GMT-03:00) Buenos Aires, Georgetown"){ echo 'selected'; } ?> data-timezone="America/Buenos_Aires" timeZoneId="23" gmtAdjustment="GMT-03:00" useDaylightTime="0" value="(GMT-03:00) Buenos Aires, Georgetown">(GMT-03:00) Buenos Aires, Georgetown</option>
									<option <?php if($timezone[0]->timezone == "(GMT-03:00) Greenland"){ echo 'selected'; } ?> data-timezone="America/Godthab" timeZoneId="24" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="(GMT-03:00) Greenland">(GMT-03:00) Greenland</option>
									<option <?php if($timezone[0]->timezone == "(GMT-03:00) Montevideo"){ echo 'selected'; } ?> data-timezone="America/Godthab" timeZoneId="25" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="(GMT-03:00) Montevideo">(GMT-03:00) Montevideo</option>
									<option <?php if($timezone[0]->timezone == "(GMT-02:00) Mid-Atlantic"){ echo 'selected'; } ?> data-timezone="Atlantic/Stanley" timeZoneId="26" gmtAdjustment="GMT-02:00" useDaylightTime="1" value="(GMT-02:00) Mid-Atlantic">(GMT-02:00) Mid-Atlantic</option>
									<option <?php if($timezone[0]->timezone == "(GMT-01:00) Cape Verde Is."){ echo 'selected'; } ?> data-timezone="Atlantic/Cape_Verde" timeZoneId="27" gmtAdjustment="GMT-01:00" useDaylightTime="0" value="(GMT-01:00) Cape Verde Is.">(GMT-01:00) Cape Verde Is.</option>
									<option <?php if($timezone[0]->timezone == "(GMT-01:00) Azores"){ echo 'selected'; } ?> data-timezone="Atlantic/Azores" timeZoneId="28" gmtAdjustment="GMT-01:00" useDaylightTime="1" value="(GMT-01:00) Azores">(GMT-01:00) Azores</option>
									<option <?php if($timezone[0]->timezone == "(GMT+00:00) Casablanca, Monrovia, Reykjavik"){ echo 'selected'; } ?> data-timezone="Africa/Casablanca" timeZoneId="29" gmtAdjustment="GMT+00:00" useDaylightTime="0" value="(GMT+00:00) Casablanca, Monrovia, Reykjavik">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
									<option <?php if($timezone[0]->timezone == "(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London"){ echo 'selected'; } ?> data-timezone="Europe/Dublin" timeZoneId="30" gmtAdjustment="GMT+00:00" useDaylightTime="1" value="(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
									<option <?php if($timezone[0]->timezone == "(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna"){ echo 'selected'; } ?> data-timezone="Europe/Amsterdam" timeZoneId="31" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
									<option <?php if($timezone[0]->timezone == "(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague"){ echo 'selected'; } ?> data-timezone="Europe/Belgrade" timeZoneId="32" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
									<option <?php if($timezone[0]->timezone == "(GMT+01:00) Brussels, Copenhagen, Madrid, Paris"){ echo 'selected'; } ?> data-timezone="Europe/Brussels" timeZoneId="33" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Brussels, Copenhagen, Madrid, Paris">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
									<option <?php if($timezone[0]->timezone == "(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb"){ echo 'selected'; } ?> data-timezone="Europe/Sarajevo" timeZoneId="34" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
									<option <?php if($timezone[0]->timezone == "(GMT+01:00) West Central Africa"){ echo 'selected'; } ?> data-timezone="Africa/Ceuta" timeZoneId="35" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="(GMT+01:00) West Central Africa">(GMT+01:00) West Central Africa</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Amman"){ echo 'selected'; } ?> data-timezone="Asia/Jerusalem" timeZoneId="36" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Amman">(GMT+02:00) Amman</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Athens, Bucharest, Istanbul"){ echo 'selected'; } ?> data-timezone="Europe/Athens" timeZoneId="37" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Athens, Bucharest, Istanbul">(GMT+02:00) Athens, Bucharest, Istanbul</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Beirut"){ echo 'selected'; } ?> data-timezone="Asia/Jerusalem" timeZoneId="38" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Beirut">(GMT+02:00) Beirut</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Cairo"){ echo 'selected'; } ?> data-timezone="Africa/Cairo" timeZoneId="39" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Cairo">(GMT+02:00) Cairo</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Harare, Pretoria"){ echo 'selected'; } ?> data-timezone="Africa/Harare" timeZoneId="40" gmtAdjustment="GMT+02:00" useDaylightTime="0" value="(GMT+02:00) Harare, Pretoria">(GMT+02:00) Harare, Pretoria</option>
									<option <?php if($timezone[0]->timezone == "GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius"){ echo 'selected'; } ?> data-timezone="Europe/Helsinki" timeZoneId="41" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Jerusalem"){ echo 'selected'; } ?> data-timezone="Asia/Jerusalem" timeZoneId="42" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Jerusalem">(GMT+02:00) Jerusalem</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Minsk"){ echo 'selected'; } ?> data-timezone="Europe/Minsk" timeZoneId="43" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Minsk">(GMT+02:00) Minsk</option>
									<option <?php if($timezone[0]->timezone == "(GMT+02:00) Windhoek"){ echo 'selected'; } ?> data-timezone="Europe/Minsk" timeZoneId="44" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="(GMT+02:00) Windhoek">(GMT+02:00) Windhoek</option>
									<option <?php if($timezone[0]->timezone == "(GMT+03:00) Kuwait, Riyadh, Baghdad"){ echo 'selected'; } ?> data-timezone="Asia/Kuwait" timeZoneId="45" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="(GMT+03:00) Kuwait, Riyadh, Baghdad">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
									<option <?php if($timezone[0]->timezone == "(GMT+03:00) Moscow, St. Petersburg, Volgograd"){ echo 'selected'; } ?> data-timezone="Europe/Moscow" timeZoneId="46" gmtAdjustment="GMT+03:00" useDaylightTime="1" value="(GMT+03:00) Moscow, St. Petersburg, Volgograd">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
									<option <?php if($timezone[0]->timezone == "(GMT+03:00) Nairobi"){ echo 'selected'; } ?> data-timezone="Africa/Nairobi" timeZoneId="47" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="(GMT+03:00) Nairobi">(GMT+03:00) Nairobi</option>
									<option <?php if($timezone[0]->timezone == "(GMT+03:00) Tbilisi"){ echo 'selected'; } ?> data-timezone="Asia/Tbilisi" timeZoneId="48" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="(GMT+03:00) Tbilisi">(GMT+03:00) Tbilisi</option>
									<option <?php if($timezone[0]->timezone == "(GMT+03:30) Tehran"){ echo 'selected'; } ?> data-timezone="Asia/Tehran" timeZoneId="49" gmtAdjustment="GMT+03:30" useDaylightTime="1" value="(GMT+03:30) Tehran">(GMT+03:30) Tehran</option>
									<option <?php if($timezone[0]->timezone == "(GMT+04:00) Abu Dhabi, Muscat"){ echo 'selected'; } ?> data-timezone="Asia/Muscat" timeZoneId="50" gmtAdjustment="GMT+04:00" useDaylightTime="0" value="(GMT+04:00) Abu Dhabi, Muscat">(GMT+04:00) Abu Dhabi, Muscat</option>
									<option <?php if($timezone[0]->timezone == "(GMT+04:00) Baku"){ echo 'selected'; } ?> data-timezone="Asia/Baku" timeZoneId="51" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="(GMT+04:00) Baku">(GMT+04:00) Baku</option>
									<option <?php if($timezone[0]->timezone == "(GMT+04:00) Yerevan"){ echo 'selected'; } ?> data-timezone="Asia/Yerevan" timeZoneId="52" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="(GMT+04:00) Yerevan">(GMT+04:00) Yerevan</option>
									<option <?php if($timezone[0]->timezone == ""){ echo 'selected'; } ?> data-timezone="Asia/Kabul" timeZoneId="53" gmtAdjustment="GMT+04:30" useDaylightTime="0" value="(GMT+04:30) Kabul">(GMT+04:30) Kabul</option>
									<option <?php if($timezone[0]->timezone == "(GMT+05:00) Yekaterinburg"){ echo 'selected'; } ?> data-timezone="Asia/Yekaterinburg" timeZoneId="54" gmtAdjustment="GMT+05:00" useDaylightTime="1" value="(GMT+05:00) Yekaterinburg">(GMT+05:00) Yekaterinburg</option>
									<option <?php if($timezone[0]->timezone == "(GMT+05:00) Islamabad, Karachi, Tashkent"){ echo 'selected'; } ?> data-timezone="Asia/Karachi" timeZoneId="55" gmtAdjustment="GMT+05:00" useDaylightTime="0" value="(GMT+05:00) Islamabad, Karachi, Tashkent">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
									<option <?php if($timezone[0]->timezone == "(GMT+05:30) Sri Jayawardenapura"){ echo 'selected'; } ?> data-timezone="Asia/Colombo" timeZoneId="56" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="(GMT+05:30) Sri Jayawardenapura">(GMT+05:30) Sri Jayawardenapura</option>
									<option <?php if($timezone[0]->timezone == "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi"){ echo 'selected'; } ?> data-timezone="Asia/Kolkata" timeZoneId="57" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
									<option <?php if($timezone[0]->timezone == "(GMT+05:45) Kathmandu"){ echo 'selected'; } ?> data-timezone="Asia/Kathmandu" timeZoneId="58" gmtAdjustment="GMT+05:45" useDaylightTime="0" value="(GMT+05:45) Kathmandu">(GMT+05:45) Kathmandu</option>
									<option <?php if($timezone[0]->timezone == "(GMT+06:00) Almaty, Novosibirsk"){ echo 'selected'; } ?> data-timezone="Asia/Almaty" timeZoneId="59" gmtAdjustment="GMT+06:00" useDaylightTime="1" value="(GMT+06:00) Almaty, Novosibirsk">(GMT+06:00) Almaty, Novosibirsk</option>
									<option <?php if($timezone[0]->timezone == "(GMT+06:00) Astana, Dhaka"){ echo 'selected'; } ?> data-timezone="Asia/Dhaka" timeZoneId="60" gmtAdjustment="GMT+06:00" useDaylightTime="0" value="(GMT+06:00) Astana, Dhaka">(GMT+06:00) Astana, Dhaka</option>
									<option <?php if($timezone[0]->timezone == "(GMT+06:30) Yangon (Rangoon)"){ echo 'selected'; } ?> data-timezone="Asia/Rangoon" timeZoneId="61" gmtAdjustment="GMT+06:30" useDaylightTime="0" value="(GMT+06:30) Yangon (Rangoon)">(GMT+06:30) Yangon (Rangoon)</option>
									<option <?php if($timezone[0]->timezone == "(GMT+07:00) Bangkok, Hanoi, Jakarta"){ echo 'selected'; } ?> data-timezone="Asia/Bangkok" timeZoneId="62" gmtAdjustment="GMT+07:00" useDaylightTime="0" value="(GMT+07:00) Bangkok, Hanoi, Jakarta">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
									<option <?php if($timezone[0]->timezone == "(GMT+07:00) Krasnoyarsk"){ echo 'selected'; } ?> data-timezone="Asia/Krasnoyarsk" timeZoneId="63" gmtAdjustment="GMT+07:00" useDaylightTime="1" value="(GMT+07:00) Krasnoyarsk">(GMT+07:00) Krasnoyarsk</option>
									<option <?php if($timezone[0]->timezone == "(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi"){ echo 'selected'; } ?> data-timezone="Asia/Hong_Kong" timeZoneId="64" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
									<option <?php if($timezone[0]->timezone == "(GMT+08:00) Kuala Lumpur, Singapore"){ echo 'selected'; } ?> data-timezone="Asia/Kuala_Lumpur" timeZoneId="65" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Kuala Lumpur, Singapore">(GMT+08:00) Kuala Lumpur, Singapore</option>
									<option <?php if($timezone[0]->timezone == "(GMT+08:00) Irkutsk, Ulaan Bataar"){ echo 'selected'; } ?> data-timezone="Asia/Irkutsk" timeZoneId="66" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Irkutsk, Ulaan Bataar">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
									<option <?php if($timezone[0]->timezone == "(GMT+08:00) Perth"){ echo 'selected'; } ?> data-timezone="Australia/Perth" timeZoneId="67" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Perth">(GMT+08:00) Perth</option>
									<option <?php if($timezone[0]->timezone == "(GMT+08:00) Taipei"){ echo 'selected'; } ?> data-timezone="Asia/Taipei" timeZoneId="68" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="(GMT+08:00) Taipei">(GMT+08:00) Taipei</option>
									<option <?php if($timezone[0]->timezone == "(GMT+09:00) Osaka, Sapporo, Tokyo"){ echo 'selected'; } ?> data-timezone="Asia/Tokyo" timeZoneId="69" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="(GMT+09:00) Osaka, Sapporo, Tokyo">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
									<option <?php if($timezone[0]->timezone == "(GMT+09:00) Seoul"){ echo 'selected'; } ?> data-timezone="Asia/Seoul" timeZoneId="70" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="(GMT+09:00) Seoul">(GMT+09:00) Seoul</option>
									<option <?php if($timezone[0]->timezone == "(GMT+09:00) Yakutsk"){ echo 'selected'; } ?> data-timezone="Asia/Yakutsk" timeZoneId="71" gmtAdjustment="GMT+09:00" useDaylightTime="1" value="(GMT+09:00) Yakutsk">(GMT+09:00) Yakutsk</option>
									<option <?php if($timezone[0]->timezone == "(GMT+09:30) Adelaide"){ echo 'selected'; } ?> data-timezone="Australia/Adelaide" timeZoneId="72" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="(GMT+09:30) Adelaide">(GMT+09:30) Adelaide</option>
									<option <?php if($timezone[0]->timezone == "(GMT+09:30) Darwin"){ echo 'selected'; } ?> data-timezone="Australia/Darwin" timeZoneId="73" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="(GMT+09:30) Darwin">(GMT+09:30) Darwin</option>
									<option <?php if($timezone[0]->timezone == "(GMT+10:00) Brisbane"){ echo 'selected'; } ?> data-timezone="Australia/Brisbane" timeZoneId="74" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="(GMT+10:00) Brisbane">(GMT+10:00) Brisbane</option>
									<option <?php if($timezone[0]->timezone == "(GMT+10:00) Canberra, Melbourne, Sydney"){ echo 'selected'; } ?> data-timezone="Australia/Canberra" timeZoneId="75" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="(GMT+10:00) Canberra, Melbourne, Sydney">(GMT+10:00) Canberra, Melbourne, Sydney</option>
									<option <?php if($timezone[0]->timezone == "(GMT+10:00) Hobart"){ echo 'selected'; } ?> data-timezone="Australia/Hobart" timeZoneId="76" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="(GMT+10:00) Hobart">(GMT+10:00) Hobart</option>
									<option <?php if($timezone[0]->timezone == "(GMT+10:00) Guam, Port Moresby"){ echo 'selected'; } ?> data-timezone="Pacific/Guam" timeZoneId="77" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="(GMT+10:00) Guam, Port Moresby">(GMT+10:00) Guam, Port Moresby</option>
									<option <?php if($timezone[0]->timezone == "(GMT+10:00) Vladivostok"){ echo 'selected'; } ?> data-timezone="Asia/Vladivostok" timeZoneId="78" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="(GMT+10:00) Vladivostok">(GMT+10:00) Vladivostok</option>
									<option <?php if($timezone[0]->timezone == "(GMT+11:00) Magadan, Solomon Is., New Caledonia"){ echo 'selected'; } ?> data-timezone="Asia/Vladivostok" timeZoneId="79" gmtAdjustment="GMT+11:00" useDaylightTime="1" value="(GMT+11:00) Magadan, Solomon Is., New Caledonia">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
									<option <?php if($timezone[0]->timezone == "(GMT+12:00) Auckland, Wellington"){ echo 'selected'; } ?> data-timezone="Pacific/Auckland" timeZoneId="80" gmtAdjustment="GMT+12:00" useDaylightTime="1" value="(GMT+12:00) Auckland, Wellington">(GMT+12:00) Auckland, Wellington</option>
									<option <?php if($timezone[0]->timezone == "(GMT+12:00) Fiji, Kamchatka, Marshall Is."){ echo 'selected'; } ?> data-timezone="Pacific/Fiji" timeZoneId="81" gmtAdjustment="GMT+12:00" useDaylightTime="0" value="(GMT+12:00) Fiji, Kamchatka, Marshall Is.">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
									<option <?php if($timezone[0]->timezone == "(GMT+13:00) Nuku'alofa"){ echo 'selected'; } ?> data-timezone="Pacific/Tongatapu" timeZoneId="82" gmtAdjustment="GMT+13:00" useDaylightTime="0" value="(GMT+13:00) Nuku'alofa">(GMT+13:00) Nuku'alofa</option>
								</select>
                            </div>
                        </div>                       
                        <div class="form-group clearfix">
                            <label class="control-label col-md-3"></label>
                            <div class="col-md-9">
                                <input type="submit" name="submit" value="save" class="btn btn-danger pull-right">                                        
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- end panel -->
        </div>
        <!-- end col-6 -->
    </div>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
