<!DOCTYPE html>
<!--[if IE 8]> 
<html lang="en" class="ie8">
    <![endif]-->
    <!--[if !IE]><!-->
    <html lang="en">
        <!--<![endif]-->
        <head>
        </head>
        <body>
            <!-- begin #content -->
            <div>
                <div>
                    <div style="background: #fff;">
                        <div style="font-size: 20px; color: #707478;">Jejo Pages</div>
                        <br />
                        <br />
                        <div style="background: #f0f3f4;margin-bottom: 20px; padding: 20px; font-size: 12px; color: #707478;">
                            <table width="100%">
                                <tr>
                                    <td width="30%">
                                        <div>
                                            <small style=" font-size: 85%;">from</small>
                                            <address style="font-style: normal;line-height: 1.42857143; margin-bottom: 5px; margin-top: 5px;">
                                                <strong style="font-size: 15px; font-weight: 600;">Jejo Pages</strong> 
                                            </address>
                                        </div>
                                    </td>
                                    <td width="30%">
                                        <div class="table-cell" style="padding-right: 20px;">
                                            <small style="font-size: 85%;">to</small>
                                            <address style="font-style: normal;line-height: 1.42857143; margin-bottom: 5px; margin-top: 5px;">
                                                <?php echo $email; ?>
                                            </address>
                                        </div>
                                    </td>
                                    <td width="40%">
                                        <div class="table-cell" style="text-align: right; padding-left: 20px;">
                                            <small style="font-size: 85%;">Invoice / <?php echo strftime("%B",$invoice_info['date']); ?> period</small>
                                            <div style="font-size: 15px; font-weight: 600;padding: 5px 0;"><?php echo strftime("%B %d, %Y",$invoice_info['date']); ?></div>
                                            <small style="font-size: 85%;"> <?php echo $invoice_info['id']; ?></small>
                                            <div>
                                                <?php ?>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="margin-bottom: 20px;">
                            <div>
                                <table style="min-height: .01%;overflow-x: auto; border-color: #e2e7eb;-webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; background: #fff;width: 100%; max-width: 100%; margin-bottom: 20px;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: left;border-color: #e2e7eb;padding: 10px 15px; color: #242a30; font-weight: 600; border-bottom: 2px solid #e2e7eb!important;background: #fff;">PLAN NAME</th>
                                            <th style="text-align: left;border-color: #e2e7eb;padding: 10px 15px; color: #242a30; font-weight: 600; border-bottom: 2px solid #e2e7eb!important;background: #fff;">TOTAL</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="border-color: #e2e7eb; padding: 10px 15px; background: #fff;line-height: 1.42857143; vertical-align: top;"> <?php echo $invoice_info['lines']['data'][0]['plan']['id'];  ?></td>
                                            <td style="border-color: #e2e7eb; padding: 10px 15px; background: #fff;line-height: 1.42857143; vertical-align: top;">$<?php  echo number_format(($invoice_info['total'] /100), 2, '.', ' '); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <table width="100%" style="background:#f0f3f4;">
                                <tr>
                                    <td width="70%">
                                        <div style="padding: 20px; font-size: 20px;font-weight: 600;  position: relative;vertical-align: middle;">
                                            <div>
                                                <small style="font-size: 12px; font-weight: 400; display: block;">SUBTOTAL</small>
                                                $<?php  echo number_format(($invoice_info['total'] /100), 2, '.', ' '); ?>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="30%" style="background: #2d353c;">
                                        <div style="float: left; width: 100%;  color: #fff; padding: 20px; text-align:center;">
                                            <small style="font-size: 28px;text-align:center; font-weight: 300;">$<?php  echo number_format(($invoice_info['total'] /100), 2, '.', ' '); ?></small>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
        </body>
    </html>