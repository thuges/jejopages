<!-- begin #content -->
<div id="content" class="content">
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
    <li><a href="javascript:;">Home</a></li>
    <li class="active">Edit User</li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header">Edit User<small></small></h1>
<!-- end page-header -->
<?php
    $error = $this->session->flashdata('error');
            if($error){
         ?>
<div class="alert alert-danger fade in m-b-15">
    <strong> :( </strong>
    <?php echo $this->session->flashdata('error'); ?>
    <span class="close fa fa-2x fa-times-circle" data-dismiss="alert"></span>
</div>
<?php }
    $this->session->unset_userdata('error');
    ?>
<div id="error_email" class="alert alert-danger fade in m-b-15" style="display:none">
    <strong> :( </strong>
    <font class='error'>Email id already exists</font>
    <span class="close fa fa-2x fa-times-circle" data-dismiss="alert"></span>
</div>
<div  id="error_password" class="alert alert-danger fade in m-b-15" style="display:none">
    <strong> :( </strong>
    <font class='error'>Please enter Passsword</font>
    <span class="close fa fa-2x fa-times-circle" data-dismiss="alert"></span>
</div>
<div class="row">
    <!-- begin col-6 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse" data-sortable-id="form-validation-1">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title">User</h4>
            </div>
            <form action="<?php echo base_url() ?>admin/user_edit/<?php echo isset($user['id']) ? $user['id'] : ''; ?>" method="post" data-parsley-validate="true"  name="demo-form">
                <div class="panel-body">
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="firstname">First Name </label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" id="firstname" name="firstname" placeholder="Enter First Name" value="<?php echo isset($user['first_name']) ? $user['first_name'] : ''; ?>" data-parsley-required="true" required />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="lastname">Last Name </label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" id="lastname" name="lastname" placeholder="Enter Last Name" value="<?php echo isset($user['last_name']) ? $user['last_name'] : ''; ?>" data-parsley-required="true" required />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="displayname">Display Name </label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" id="displayname" name="displayname" placeholder="Enter Display Name" data-parsley-required="true" value="<?php echo isset($user['display_name']) ? $user['display_name'] : ''; ?>"  required />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="email">Email </label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" id="email" name="email" data-parsley-type="email" value="<?php echo isset($user['email']) ? $user['email'] : ''; ?>" placeholder="Enter Email" data-parsley-required="true" required />
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="password">Password </label>
                        <div class="col-md-9">
                            <div class="input-group generate-password">
                              <input  data-placement="after" class="form-control m-b-5" type="password" id="password-indicator-default" name="password"  placeholder="Enter Password"/> 
                                <span class="input-group-addon" id="show_pass">
                                    <i class="fa fa-eye"></i>
                                </span>                                
                            </div>
                            <div id="passwordStrengthDiv" class="is0 m-t-5"></div>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3" for="rol">Role</label>
                        <div class="col-md-3">
                            <select class="combobox" name="role" required >
                                <option value="">Select user role</option>
                                <option value="1" <?php if(isset($user['role']) and $user['role'] == 1) { echo 'selected'; } ?> >User</option>
                                <option value="2" <?php if(isset($user['role']) and $user['role'] == 2) { echo 'selected'; } ?> >Administrator</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="control-label col-md-3"></label>
                        <div class="col-md-9">
                            <input type="submit" name="submit" value="save" class="btn btn-danger pull-right">                                        
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-6 -->
</div>
<style type="text/css">
    
  .input-group-addon {
  -webkit-order: 2;
  order: 2;}  
</style>

