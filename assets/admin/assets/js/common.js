    $(document).ready(function() {
              App.init();
              Dashboard.init();
              FormPlugins.init();
              FormWizardValidation.init();
    });

   $(".sidebar-menu").on('click', function(){
        $(this).removeClass('active');
    }); 

  $(".user_table").on('click', '.deleteuser', function() {
       var id = $(this).attr("data-id");
       swal({
           title: "Are you sure?",
           text: "User Delete",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: '#DD6B55',
           confirmButtonText: 'Yes, delete ',
           cancelButtonText: "No, cancel ",
           closeOnConfirm: false,
           closeOnCancel: false
       },
       function(isConfirm) {
           if (isConfirm) {
               window.location.href = jsbaseurl + 'index.php/admin/user_delete/' + id;
           } else {
               swal("Cancelled", "Your data is safe :)", "error");
           }
       });

   });




  $(".amazon_table").on('click', '.delete_amazonmws', function() {
       var id = $(this).attr("data-id");
       swal({
           title: "Are you sure?",
           text: "Account Delete",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: '#DD6B55',
           confirmButtonText: 'Yes, delete ',
           cancelButtonText: "No, cancel ",
           closeOnConfirm: false,
           closeOnCancel: false
       },
       function(isConfirm) {
           if (isConfirm) {
               window.location.href = jsbaseurl + 'index.php/admin/amazon_account_delete/' + id;
           } else {
               swal("Cancelled", "Your data is safe :)", "error");
           }
       });

   });
 

$(document).ready(function() {

    //timezone selected
    var timezone =  $(this).find(':selected').attr('data-timezone');
    $('#timezone_name').val(timezone);

    $('#user-data-table').DataTable({
       "pageLength": 100,
       "ordering": false
   });  

   $('#amazonmws-data-table').DataTable({
       "pageLength": 100,
       "ordering": false
   });  


   $('#invoice_table').DataTable({
       "pageLength": 100,
       "ordering": false
   });  

    columns = [
        { "data": "date_time"},
        { "data": "user"},
        { "data": "action" },
        { "data": "ip" },        
    ];

    var UserLogTable = $('#log-data-table').DataTable({
    "columns": columns,
        "pageLength": 100,
        "oSearch": { "bSmart": false, "bRegex": true },
        "bServerSide": true,
        "processing": true,
        "ajax":{
            "url": jsbaseurl +'admin/logs-records',
            "type": "GET"
        },
        "initComplete": function(settings, json) {
            var info = this.api().page.info();
            // alert(info.recordsTotal);
            $.fn.dataTable.ext.errMode = 'none';
        },
        "bLengthChange": true, // can change the length
        "lengthMenu": [[10, 20, 30, 40, 50, 100], [10, 20, 30, 40, 50, 100]],

        "order": [[ 0, "desc" ]],        
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) { 

                if ( aData['action'] == "Logout" ) {
                        $(nRow).addClass('active');
                    } else if ( aData['action'] == "Unsuccessful login" ) {
                        $(nRow).addClass('warning');
                    } else if( aData['action'] == "Successful login" ){
                        $(nRow).addClass('success');
                    }
                }
    });   
});  

$('#show_pass').on('click', function(event) {

    if($('#show_pass').find('i.fa-eye').length !== 0){
        $('#password-indicator-default').attr('type', 'text');    
        $('#show_pass').children('i').removeClass('fa fa-eye');
        $('#show_pass').children('i').addClass('fa fa-eye-slash');
    } else {
       $('#password-indicator-default').attr('type', 'password');    
       $('#show_pass').children('i').removeClass('fa-eye-slash');
       $('#show_pass').children('i').addClass('fa-eye');
    }
 
});

//msw redirect tab
$('#amazonbtn').click(function() {    
    ajax_user_log('Step 1 - Clicked Button Open MWS authentication page');           
    setTimeout(function(){
        $('#step2').trigger('click');  
    },1000);            
});

$('#nextstep').click(function() {        
    ajax_user_log('Step 2 - Clicked Button proceed to step 3');        
    setTimeout(function(){
        $('#step3').trigger('click');  
    },1000);            
});

$('#nextStepAccess').click(function() {
   ajax_user_log('Step 3 - Clicked Button proceed to step 4');        
   setTimeout(function(){
       $('#step4').trigger('click');  
   },1000);            
});
 
//timezone change set value
$('#timezone').on('change',function(event) { 
    var timezone =  $(this).find(':selected').attr('data-timezone');
    $('#timezone_name').val(timezone);
});  

// MWS LOG 
function ajax_user_log(user_action=''){
  $.ajax({
    url: jsbaseurl + 'index.php/admin/ajax_user_log',
    type: 'POST',
    dataType: 'json',
    data: {action:user_action},
  })
}