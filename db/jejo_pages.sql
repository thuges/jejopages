-- phpMyAdmin SQL Dump
-- version 4.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2017 at 05:43 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jejo_pages`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `role` tinyint(1) NOT NULL DEFAULT '1',
  `display_name` varchar(200) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `role`, `display_name`, `first_name`, `last_name`, `email`, `password`, `status`) VALUES
(1, 2, 'admin', 'login', 'admin', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 1),
(13, 2, 'prakash', 'prakash', 'vasavada', 'prakash.v.php@gmail.com', 'b0510e64d7144eb40ff84f2c20a1a29f', 1);

-- --------------------------------------------------------

--
-- Table structure for table `amazon_mws_accounts`
--

CREATE TABLE IF NOT EXISTS `amazon_mws_accounts` (
  `id` int(11) NOT NULL,
  `seller_id` varchar(50) NOT NULL,
  `store_name` varchar(200) DEFAULT NULL,
  `authtoken` varchar(200) DEFAULT NULL,
  `keyid` varchar(255) NOT NULL,
  `marketplaceid` varchar(255) NOT NULL,
  `secretkey` varchar(255) NOT NULL,
  `serviceurl` varchar(200) NOT NULL,
  `marketpalce_name` varchar(100) DEFAULT NULL,
  `marketplace_img` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `date_time`, `user`, `action`, `ip`) VALUES
(1, '2017-07-03 05:49:14', 'admin', 'Logout', '::1'),
(2, '2017-07-03 05:49:24', 'admin', 'Successful login', '::1'),
(3, '2017-07-03 05:49:35', 'admin', 'Logout', '::1'),
(4, '2017-07-03 05:49:53', NULL, 'Unsuccessful login', '::1'),
(5, '2017-07-03 05:50:04', 'admin', 'Successful login', '::1'),
(6, '2017-07-03 05:56:05', 'admin', 'Logout', '::1'),
(7, '2017-07-03 05:56:15', 'prakash', 'Successful login', '::1'),
(8, '2017-07-03 05:56:33', 'prakash', 'Logout', '::1'),
(9, '2017-07-03 05:56:40', 'admin', 'Successful login', '::1'),
(10, '2017-07-03 06:53:42', 'admin', 'Logout', '::1'),
(11, '2017-07-03 06:53:47', 'admin', 'Successful login', '::1'),
(12, '2017-07-03 07:09:08', 'admin', 'Logout', '::1'),
(13, '2017-07-03 07:11:46', 'admin', 'Successful login', '::1'),
(14, '2017-07-03 07:52:38', 'admin', 'Logout', '::1'),
(15, '2017-07-04 10:16:46', 'admin', 'Successful login', '::1'),
(16, '2017-07-04 12:17:32', 'admin', 'Successful login', '127.0.0.1'),
(17, '2017-07-04 01:47:00', 'admin', 'Successful login', '::1'),
(18, '2017-07-04 02:09:58', 'admin', 'Logout', '::1'),
(19, '2017-07-04 02:22:48', 'admin', 'Successful login', '::1'),
(20, '2017-07-04 02:22:51', 'admin', 'Logout', '::1'),
(21, '2017-07-04 02:24:36', 'admin', 'Successful login', '::1'),
(22, '2017-07-04 02:47:02', 'admin', 'Successful login', '::1'),
(23, '2017-07-04 02:47:49', 'admin', 'Successful login', '127.0.0.1'),
(24, '2017-07-04 04:43:42', 'admin', 'Successful login', '::1'),
(25, '2017-07-05 10:16:20', 'admin', 'Successful login', '::1'),
(26, '2017-07-05 01:42:02', 'admin', 'Successful login', '::1'),
(27, '2017-07-05 03:43:50', 'admin', 'Successful login', '::1'),
(28, '2017-07-05 04:04:39', 'admin', 'Logout', '::1'),
(29, '2017-07-05 04:04:44', 'admin', 'Successful login', '::1'),
(30, '2017-07-05 04:14:22', 'admin', 'Logout', '::1'),
(31, '2017-07-05 04:14:29', 'admin', 'Successful login', '::1'),
(32, '2017-07-06 09:44:28', 'admin', 'Successful login', '::1'),
(33, '2017-07-06 01:24:12', NULL, 'Unsuccessful login', '::1'),
(34, '2017-07-06 01:24:20', NULL, 'Unsuccessful login', '::1'),
(35, '2017-07-06 01:24:28', 'admin', 'Successful login', '::1'),
(36, '2017-07-06 01:24:42', 'admin', 'Logout', '::1'),
(37, '2017-07-06 01:29:07', 'admin', 'Successful login', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `timezone`
--

CREATE TABLE IF NOT EXISTS `timezone` (
  `id` int(11) NOT NULL DEFAULT '1',
  `timezone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timezone`
--

INSERT INTO `timezone` (`id`, `timezone`) VALUES
(1, '(GMT-09:00) Alaska');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amazon_mws_accounts`
--
ALTER TABLE `amazon_mws_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `amazon_mws_accounts`
--
ALTER TABLE `amazon_mws_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `datetime` datetime DEFAULT CURRENT_TIMESTAMP,
  `plan_name` varchar(100) DEFAULT NULL,
  `plan_amount` int(11) DEFAULT NULL,
  `stripe_customer_id` varchar(200) DEFAULT NULL,
  `stripe_subscription_id` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `timezone` ADD `timezone_name` VARCHAR(100) NULL AFTER `timezone`;